package com.example.jacobplumb.test;

import android.app.Activity;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Scanner;

public class MainActivity extends Activity {

    public void loadPeople(View view){
        Resources res = getResources();
        InputStream is = res.openRawResource(R.raw.people);
        Scanner scanner = new Scanner(is);
        StringBuilder stringBuilder = new StringBuilder();
        while (scanner.hasNext()){
            stringBuilder.append(scanner.nextLine());
        }
        parseJson(stringBuilder.toString());
    }

    private void parseJson(String json){
        StringBuilder builder = new StringBuilder();
        try {
            JSONObject root = new JSONObject(json);
            JSONArray people = root.getJSONArray("people");

            for (int i = 0; i < people.length(); i++){
                JSONObject person = people.getJSONObject(i);
                builder.append(person.getInt("age"))
                        .append(":")
                        .append(person.getInt("weight"))
                        .append("\n\n");
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }

        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(builder.toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
